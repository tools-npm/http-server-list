const gulp = require("gulp");
const cleanCSS = require("gulp-clean-css");
const rename = require("gulp-rename");
const clean = require('gulp-clean');
const watch = require("gulp-watch");
const changed = require("gulp-changed");
const minifyjs = require("gulp-js-minify");
const uglify = require('gulp-uglify');
const argv = require('yargs').argv;
const log = require('fancy-log');

// const assets = "./public";
const assets = argv.assets;
log("ASSETS", assets);

gulp.task("watch-css", function () {
    
    return gulp.src( [ "!" + assets + "/**/*.min.css", assets + "/**/*.css"])
        .pipe(watch([ "!" + assets + "/**/*.min.css", assets + "/**/*.css"], {base: assets}))
        .pipe(cleanCSS({compability: "ie8"}))
        .pipe(rename({
            suffix: ".min",
            extname: ".css"
        }))
        .pipe(gulp.dest(function (file){
            // console.log(file);
            return file.base;
        }));
})
gulp.task("watch-js", function () {

    log("assets: ", assets);

    return gulp.src( [ "!" + assets + "/**/*/czmv.js", "!" + assets + "/**/cypress/**", assets + "/**/*.js"])

        .pipe(watch([ "!" + assets + "/**/*/czmv.js", "!" + assets + "/**/cypress/**" , assets + "/**/*.js"], {base: assets}))
        .pipe(uglify().on("error", function (error) {
            console.log(error);
        }))
        .pipe(rename({
            basename: "czmv",
            extname: ".js"
        }))
        .pipe(gulp.dest(function (file){
            // console.log(file.base);
            return file.base;
        }));
})

gulp.task("clean-txt", function(){
    return gulp.src(assets + "/**/*.txt", {read: false})
        .pipe(clean({force: true}));
})

gulp.task("clean-min-css", function(){
    return gulp.src(assets + "/**/*.min.css", {read: false})
        .pipe(clean({force: true}));
})


gulp.task("default", ["watch-css", "watch-js"]);